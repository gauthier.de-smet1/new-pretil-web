+++
title = 'Sos'
date = 2023-12-01T10:20:24+01:00
draft = false
+++

# Projet

## Auto-organisation adaptative et résiliente de flottes de robots hétérogènes par émergence collective pour une mission

![drones-foret](../img/SOS.jpg)

la révolution de la robotique et de l'intelligence artificielle rend désormais possible le rêve d'applications ambitieuses et d'envergure. En particulier la mise en oeuvre de flottes de robots intelligemment gérée permettrait d'apporter des solutions innovantes à de nombreux problèmes concrets. Parmis ceux-ci, le projet SOS (Self-Organizing, Smart and safe heterogeneous robots fleet by collective emergence for a mission) s'interesse à la détéction de feux de forêts à l'aide d'une flotte de robots aériens et terrestres.

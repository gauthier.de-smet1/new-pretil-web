+++
title = 'Robucar'
date = 2023-10-10T10:22:51+02:00
draft = false
+++

# Robucars

![Robucar](../img/terrestres/robucar.png)

L'optimisation des transports de demain passera par l'emploi de véhicules dits intelligents. Des expériences ont déjà été menées pour la réalisation de trains de poids lourds capables de se suivre en toute sécurité et ceci à distance fixe, le lien étant télémétrique au lieu d'être physique.
Cette capacité "d'accrochage télémétrique" entre véhicule maitre (leader) et véhicule suiveur (esclave) peut-être transposée à de nombreuses applications : transbordeurs de containers, exploitation minière / terrassement, transports en commun du futur...)
Afin d'étudier les comportements possibles de ces trains de véhicules intelligents dans différentes situations, normales et dégradées.
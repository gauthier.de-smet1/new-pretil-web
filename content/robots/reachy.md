+++
title = 'Reachy'
date = 2023-11-24T11:11:41+02:00
draft = false
+++

![reachy](../img/humanoides/reachy.png)

Reachy is an expressive open-source humanoid platform programmable with Python and ROS. He is particularly good at interacting with people and manipulating objects.

* Objects Manipulation
* Remote teleoperation with VR
* Available with mobility
* Interactivity
* Open source Hardware
* Artificial Intelligence
* Evolutive & Modular
* Easy to use Python API
* Fully programmable
* Powered by ROS
* Customizable
* Made in France 🇫🇷
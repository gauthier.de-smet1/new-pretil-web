+++
title = 'F1tenth'
date = 2023-10-09T14:54:25+02:00
draft = false
+++

# Jumeaux Physiques F1tenth

![F1tenth](../img/terrestres/f1tenth.png)

Le programme et la Fondation F1TENTH promeuvent l'éducation STEM de la manière suivante :

Développe du matériel d'enseignement et de formation pour les cours universitaires et de la maternelle à la 12e année sur la robotique, les véhicules autonomes et sur les bases des systèmes autonomes. Cela comprend les principes fondamentaux de la perception des robots, de la vision par ordinateur, de la localisation et de la cartographie des robots, de la planification des mouvements et du contrôle sûr. Ceux-ci sont actuellement utilisés et enseignés par plus de 60 universités qui font partie de la communauté F1Tenth. Cela inclut des universités telles que l'Université de Californie à San Diego, l'Université Clemson, l'Université d'État de l'Oregon, l'Université de Virginie, l'Université Lehigh, l'Université de Pennsylvanie et bien d'autres. Pour plus de détails, voir https://f1tenth.org/learn
Organise des événements éducatifs tels que des ateliers, des tutoriels, des séminaires et des panels sur la construction de véhicules autonomes sûrs. Ces événements sont organisés lors de conférences d'ingénierie et d'informatique accréditées par l'IEEE et l'ACM.
Organise des événements communautaires tels que les courses autonomes internationales. Nous avons accueilli plus de 11 compétitions internationales de courses autonomes à New York, Pittsburgh, au Portugal, en Corée du Sud, en Italie, au Canada, à Vienne, etc. Pour plus de détails, voir https://f1tenth.org/race
Nous développons et partageons des conceptions de logiciels et de matériel open source pour que quiconque puisse créer librement des plates-formes robotiques autonomes qui peuvent détecter, planifier et agir. Pour plus de détails, voir 
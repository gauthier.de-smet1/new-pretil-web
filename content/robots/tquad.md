+++
title = 'Tquad'
date = 2023-10-09T14:55:38+02:00
draft = false
+++

# T-Quad

![Tquad](../img/terrestres/tquad.jpg)

T-Quad peut être utilisé dans différentes configurations.

Nous utilisons 4 roues Mecanum. Avec ces dernières, le robot peut non seulement avancer, reculer et tourner, mais il peut aussi se translater vers la gauche ou vers la droite ! En fait, en fonction de la combinaison des vitesses de rotation des roues, tout type de mouvement est possible.

La version avec Raspberry Pi. Elle est utilisée pour ajouter des fonctionnalités de communication performantes (Wifi, streaming vidéo en temps-réel) et de contrôle de haut niveau (évitement d'obtacles, reconnaissance d'objets,...) en langage Python.

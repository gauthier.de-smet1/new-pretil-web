+++
title = 'Renault Zoe'
date = 2023-11-24T11:00:58+02:00
draft = false
+++

### PRETIL dispose de: 
 * **2 Zoés** robotisées téléoperables. (ROS1 & ROS2)
 * Une **troisieme** Zoé est prévue courant 2024/2025

![zoes](../img/terrestres/zoe2.jpg)

{{< youtube mwJj8csgNEA >}}

{{< youtube NVtRQ9E7uH4 >}}
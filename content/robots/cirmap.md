+++
title = 'Cirmap'
date = 2023-10-10T10:23:27+02:00
draft = false
+++

# CIRMAP – CIrcular economy via customisable furniture with Recycled MAterials for public Places

* Total budget : € 6.98 Million
* EU funding : € 4,18 Million
* Duration: 36 months (Avril 2020 – March 2023)

![logo](../img/impression-beton/cirmap-logo.png)

![mobilier](../img/impression-beton/cirmap.jpg)
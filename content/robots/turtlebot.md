+++
title = 'Turtlebot'
date = 2023-10-09T14:51:20+02:00
draft = false
+++

![turtlebot](../img/terrestres/turtlebot.jpg)

TurtleBot3 Burger est un robot mobile nouvelle génération qui est modulaire, compact et personnalisable, tout comme son acolyte le Waffle Pi. Ce nouveau robot programmable de Robotis comprend un Raspberry Pi 3, avec lequel le robot peut faire marcher ROS et Ubuntu (le premier open-source système d’opération pour PCs, objets connectés…). Il possède un logiciel qui marche sous Apache 2.0 et fonctionne totalement en Open Source. Un autre aspect intéressant est son fonctionnement en open CR. Ce qui veut dire qu'il est possible d'imprimer les éléments en 3D où de les acheter prêtes pour le montage. Turtlebot3 Burger marche avec les dernières versions d'Ubuntu Linux (13.04.2 LTS) et ROS (Kinetic). Il utilise les interfaces d’ordinateur à carte unique avec un tableau de contrôle. En plus d’employer ROS pour contrôler le robot, tu peux également programmer des comportements supplémentaires en utilisant les fonctions C/C+ et les bibliothèques d’Arduino.
+++
title = 'Kuga'
date = 2023-10-10T10:59:23+02:00
draft = false
+++

# CIRMAP

Le projet **CIRMAP** est un projet Interreg NWE qui vise à développer de nouvelles solutions pour la conception et la fabrication de mobilier urbain par impression 3D avec des matériaux écologiques. L’impression 3D (3DP) se développe rapidement dans le domaine de la construction. Réalisation rapide de géométries personnalisées ou complexes, optimisation des formes… sont parmi les principaux avantages du procédé qui pourrait permettre des avancées significatives

![cirmap](../img/impression-beton/kuka.jpg)

Le projet est réalisé dans la continuité de plusieurs travaux de recherche coopérative, en particulier du projet SeRaMCo (Interreg NWE) et du projet MATRICE (FEDER région Hauts de France). Le projet SeRaMCo a déjà exploré la réutilisation de granulats de béton recyclés pour la fabrication de produits préfabriqués en béton à l’aide de la technologie de moulage traditionnelle. Plus spécifiquement dans la production de meubles en béton fabriqués avec du béton coulé utilisant des granulats de béton recyclé. Le projet MATRICE a, quant à lui, fourni une preuve de concept pour l’application de l’impression 3D de béton dans la construction. En particulier, des formes spécifiques personnalisées, des mortiers d’impression et des imprimantes 3D ont été développés dans le cadre de ce projet et sont maintenant opérationnels pour l’impression 3D de produits en béton de tailles compatibles avec le mobilier urbain. La combinaison des connaissances acquises dans ces deux projets permettrait de concevoir et d’imprimer du mobilier urbain personnalisé avec du béton recyclé.
+++
title = "2rm Journée Véhicules autonomes"
date = "2023-10-05T21:29:20+02:00"
tags = ["CNRS", "CRIStAL", "PRETIL", "LOCSP", "2rm", "Saclay", "gdr robotique"]
categories = ["CNRS","CRIStAL","PRETIL","GDR","demonstration"]
banner = "img/banners/2rm.png"
authors = ["Gauthier de Smet"]
+++
# JOURNÉE OUTILS LOGICIELS ET MATÉRIELS POUR LA RECHERCHE SUR LES VÉHICULES TERRESTRES AUTONOMES

La diversification des systèmes de conduite automatisée (navettes autonomes, essais de robots-taxis, robots-livreurs, aautonomie des voitures particulières...) montre que la conduite autonome est un sujet de recherche très dynamique, partagé entre les laboratoires et les entreprises.
L’objectif de cette journée, organisée par l’ENS Paris-Saclay, le Réseau métier des Roboticiens et Mécatroniciens (2RM / CNRS) et le GT2 (véhicules terrestres autonomes) du GDR robotique (CNRS), est d’échanger sur les outils déployés ou développés par les laboratoires de recherche pour mener des expériences sur les problématiques propres des véhicules terrestres autonomes. L'inscription est gratuite mais obligatoire.
Rendez-vous le jeudi 5 octobre 2023 à l'ENS Paris-Saclay.

## LE PROGRAMME

Thèmes scientifiques et techniques : voitures autonomes, perception, analyse de scène, conduite automatisée.
La diversification des systèmes de conduite automatisée (service de navettes autonomes, essais de robots-taxis, robots-livreurs, augmentation des niveaux d’autonomie des voitures particulières) montre que la conduite autonome pour les transports reste un sujet de recherche très dynamique, partagé entre les laboratoires et les entreprises.
L’objectif de cette journée, organisée par l’ENS Paris-Saclay, le Réseau métier des Roboticiens et Mécatroniciens (2RM / CNRS) et le GT2 (véhicules terrestres autonomes) du GDR robotique (CNRS), est d’échanger à propos des outils déployés ou développés par les laboratoires de recherche pour mener des expériences sur les problématiques propres des véhicules terrestres autonomes.

Force de constater une forte émergence de moyens expérimentaux autour des axes suivants :

* la simulation,
* les capteurs,
* les environnements de développement,
* les architectures matérielles,
* les voitures à échelle réduite,
* les prototypes à échelle réelle (ex. robotisation de véhicules de série).

Par ailleurs, les méthodologies basées sur l’apprentissage profond ont un besoin accru de bases de données issues de ces outils et leur accessibilité à la communauté scientifique.

La journée sera organisée en 3 temps :

* Une session plénière ;
* Des sessions parallèles orientées sur les outils technologiques et sur la recherche appliquée ;
* Des démonstrations de prototypes taille réelle et réduite et d’outils logiciels.

Les thématiques abordées :

* la simulation,
* les capteurs,
* les environnements de développement,
* les architectures matérielles,
* les voitures à échelle réduite,
* les prototypes à échelle réelle (ex. robotisation de véhicules de série),
* les données partagées pour l'apprentissage.

Les personnes souhaitant présenter leurs travaux sous forme de poster (format A0, template) peuvent les déposer ici ou présenter leur plateforme lors des démonstrations ou envoyer, par e-mail, leur proposition aux organisateurs.

Mise à disposition des contributions.
Le dépôt Git accueillera les contributions mises à disposition.

[Article Paris Saclay](https://ens-paris-saclay.fr/agenda/journee-outils-logiciels-et-materiels-pour-la-recherche-sur-les-vehicules-terrestres)
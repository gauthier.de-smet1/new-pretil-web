+++
title = "Drones Parrot ANAFI Ai"
date = "2024-01-08T21:29:20+02:00"
tags = ["CNRS", "CRIStAL", "PRETIL", "LOCSP", "Drones", "BeProAct"]
categories = ["CRIStAL","PRETIL","drones","demonstrateurs"]
banner = "img/banners/news.jpg"
authors = ["Gauthier de Smet"]
+++

<style>
    .more{
        font-weight:bolder;
        text-align:center;
    }
</style>

# Drones Parrot ANAFI Ai 

## Réception de 2 drônes ANAFI Ai de chez Parrot

![image](https://www.parrot.com/assets/s3fs-public/styles/lg/public/2021-07/anafi-ai-main-and-stereo-cameras-fr.jpg)

<a class="more" href="https://www.parrot.com/fr/domaines-application/inspection-de-pont-avec-anafi-ai" target="_blank">En Savoir plus</a>
+++
title = "2rm ANF ROS2"
date = "2023-10-05T21:29:20+02:00"
tags = ["CNRS", "CRIStAL", "PRETIL", "LOCSP", "2rm", "ANF", "ROS2"]
categories = ["CNRS","CRIStAL","PRETIL","ROS2","demonstration"]
banner = "img/banners/2rm.png"
authors = ["Gauthier de Smet"]
+++

# Action Nationale de Formation 2023 à ROS 2 sur Lille

## Où ?

Au laboratoire CRIStAL sur Lille

Adresse : UMR CNRS 9189 CRIStAL Cité Scientifique - Bâtiment ESPRIT Avenue Henri Poincaré 59650 Villeneuve d’Ascq FRANCE

Pour se rendre à CRIStAL en transport en commun, prendre la ligne 1 du métro et descendre à 4 cantons Stade Pierre Mauroy (terminus de la ligne). La durée de parcours à partir des gares Lille Flandres et Lille Europe est d'environ 15 minutes.

Le bâtiment ESPRIT est situé à côté de l'école Polytech Lille et accessible à la sortie du métro par l'avenue Henri Poincaré.

## Quoi ?

Action de formation à ROS 2 pour toute personne amenée à mettre en oeuvre ROS 2 sur des robots.

Il s'agit de la seconde édition de cette formation, la première s'étant déroulée en 2022 à Fréjus.

Le Réseau de Métier des Roboticiens et Mécatronciens (²RM) qui regroupe les personnels techniques dans les laboratoires concernés par la robotique et ou la mécatronique a identifié lors d’une enquête un fort besoin de formation sur ce logiciel, en particulier avec la sortie d’une nouvelle version ROS2 depuis 2020.

Le middleware ROS est aujourd’hui un ensemble logiciel incontournable dans la recherche en robotique au niveau international. Il permet l’échange de procédés entre les équipes de recherches mais aussi avec les acteurs de l’industrie. C’est un outil fédérateur en perpétuelle évolution, supporté par un très grand nombre de contributeurs et qui apporte des solutions pertinentes pour des applications robotiques..

La version ROS2 constitue une évolution majeure de cet environnement, apportant des changements significatifs. En plus d’une formation initiale, cette action permet d’accompagner des personnes vers cette nouvelle version.

Objectifs de l'action de formation
Les objectifs sont de connaître le logiciel ROS2 et son environnement, y compris sa composante ROS Control dédiée aux contrôle temps-réel des robots, ainsi que d'avoir une première expérience pratique de son utilisation.

À l’issue de la formation les participant·es seront capables de déployer des composants logiciels basés sur ROS2 sur des capteurs, actionneurs ou robots complets ainsi que de développer des nouveaux composants intégrant les travaux de recherche de leurs équipes

### Contenu de la formation

Connaissance de l’écosystème de ROS2
Installation de ROS 2 sur sa machine
Apprentissage des concepts de base : paramètres, noeuds, topics, actions, services, messages
Utilisation des outils de base : commandes, système de build, roslaunch, rosbags, etc.
Représentation des transformations 3D
Développement d’un code de contrôle d’un actionneur ou d’un capteur en utilisant ROS2 et ROS Control
Mise en œuvre avec le simulateur Gazebo

### Comité d'organisation

Le comité d'organisation est composé de :

Laurent Barbé (iCube, Strasbourg)
Stéphane Bonnet (Heudiasyc, Compiègne)
Gérald Dherbommez (Crystal, Lille)
Matthieu Herrb (LAAS, Toulouse)
Laurent Malaterre (IP, Clermont-Ferrand)
François Marmoiton (IP, Clermond-Ferrand)
Robin Passama (LIRMM, Montpellier)
Olivier Stasse (LAAS, Toulouse)
Philippe Zanne (iCube, Strasbourg)

## Le comité local d'organisation de CRIStAL qui assure l'accueil des stagiaires est composé de

Gérald Dherbomez
Gauthier De Smet
Mario Sanz Lopez
Maxime Duquesne

## Qui ?

La formation est destinée à un public d'ingénieurs, techniciens, chercheurs, doctorants de la communauté scientifique académique en robotique.

## Comment ?

La formation et l'hébergement pendant les 3 nuits est pris en charge par la formation permanente et la Mission pour les Initiatives Transverses et Inter-disciplinaires du CNRS. Seul le voyage reste à la charge de votre laboratoire.

Cette prise en charge n'est possible que pour les personnels rémunérés par un organisme public de recherche, (CNRS, Universités, INSERM, INRAE, INRIA, CEA,...).

[2rm ANF ROS2](https://wiki.2rm.cnrs.fr/action/edit/AnfRos2_edition2023Lille)

![IMG-test2](../../../../../img/news/hall_1.png)
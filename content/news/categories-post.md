+++
title = "fête de la science"
date = "2023-10-07T21:29:20+02:00"
tags = ["CNRS", "CRIStAL", "PRETIL", "LOCSP", "COBRA", "PIRVI"]
categories = ["CNRS","CRIStAL","PRETIL","PIRVI","Demonstration"]
banner = "img/banners/CNRS-science.jpg"
authors = ["Gauthier de Smet"]
+++

# Visites insolites CNRS 2023

Forte de ses 34 équipes de recherche, le Centre de Recherche en Informatique, Signal et Automatique de Lille regroupe différentes compétences complémentaires permettant d’aborder les grands sujets sociétaux de l’informatique comme l’intelligence artificielle sous ses différentes facettes, la santé numérique, la réalité virtuelle ou encore la robotique.

Lors de cette visite, les équipes du CRIStAL vous proposent de découvrir les grands défis technologiques de l’informatique à la croisée de la robotique et de la réalité virtuelle ! Sur les plateformes de recherche en robotique et en réalité virtuelle & augmentée du laboratoire CRIStAL, des chercheur·se·s, ingénieur·e·s et doctorant·e·s développent les nouveaux algorithmes qui verront le jour dans les futurs produits technologiques (dispositifs de visualisation et d’interactions, robots mobiles, drones, véhicules autonomes, robots médicaux). Découvrez en avant-première et en immersion les plateaux techniques qui développent ces futures innovations !

[Article CNRS](https://www.hauts-de-france.cnrs.fr/fr/evenement/les-visites-insolites-du-cnrs-2023-au-cristal)
